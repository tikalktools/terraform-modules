#!/bin/sh


path=$1
system=$2
module=$3

version=$(cat "${path}${system}/${module}/module.json" | jq -r '.version')
file_name="${module}-${system}-${version}.tgz"


tar -cvzf $file_name -C "modules/${system}/${module}" --exclude=./.terraform --exclude=./.git --exclude=./.terraform.lock .
response=$(curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
    --upload-file $file_name ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${module}/${system}/${version}/file)

msg=$(echo $response | jq '.message')
if [ $msg = "null" ]; then
 echo "Fail to push to terraform registry. Exiting!"
 exit 1;
fi