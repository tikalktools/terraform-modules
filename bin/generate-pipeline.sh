#!/bin/bash

is_new(){
  equal=0
  path=$1
  tf_system=$2
  tf_module=$3
  tf_version=$(cat "${path}${tf_system}/${tf_module}/module.json" | jq -r '.version')
  curl -XGET -H "Authorization: Bearer ${CI_JOB_TOKEN}" \
  "${CI_API_V4_URL}/packages/terraform/modules/v1/${CI_PROJECT_NAMESPACE}/${tf_module}/${tf_system}/versions" \
  | jq -r '.modules | .[] | .versions | .[] | .version' > t.txt
  
  while read ver;
    do
      if [ "$ver" = "$tf_version" ]; then
       equal=1
      fi
    done < t.txt
  if [ $equal = 1 ]; then
    return 1
  else
    return 2
  fi
}



for tf_sys_path in $(ls -d modules/*);
do
tf_system=$(echo $tf_sys_path | xargs -L1 basename | awk '{print tolower($0)}')
export i=0
for tf_mod_path in $(ls -d $tf_sys_path/*);
do
module=$(echo $tf_mod_path | xargs -L1 basename | awk '{print tolower($0)}')
  if ! (test -f "${tf_mod_path}/module.json"); then
    echo "no module.json file found for ${tf_mod_path}!" >&2
    continue
  fi

  is_new "modules/" $tf_system $module
  res=$?

  if [ $res = 2 ]; then
  ((i=i+1))
	cat << EOF
${tf_system}_${module}_test:
    image: "registry.gitlab.com/itamar.gur/tool-box:latest"
    stage: test
    script:
        - cd "modules/${tf_system}/${module}"
        - terraform init
        - terraform validate

${tf_system}_${module}_deploy:
    image: "registry.gitlab.com/itamar.gur/tool-box:latest"
    stage: deploy
    script:
      - bin/deploy.sh "modules/" ${tf_system} ${module}
    only:
      - main
    needs: ["${tf_system}_${module}_test"] 
EOF
fi
done
done

if [ "$i" -le "0" ]; then
echo "no jobs needed. creating default no_chages" >&2
	cat << EOF
no_changes:
    stage: deploy
    script:
      - echo "no changes in modules"
    
EOF
fi