locals {
  sa_name            = var.service_account_name != null ? var.service_account_name : random_id.sa.b64_std
  sa_annotation      = "eks.amazonaws.com/role-arn"
  sa__set_annotation = "serviceAccount.annotations.\\eks\\.amazonaws\\.com/role-arn"
  namespace          = (var.irsa_namespace_wildcard) ? "*" : var.namespace
  role_subject       = "system:serviceaccount:${local.namespace}:${local.sa_name}"
  create_sa          = (var.create_service_account && var.namespace != null)

  provider_url = var.provider_url != null ? var.provider_url : data.aws_eks_cluster.cluster.identity[0].oidc[0].issuer

  cluster_data_count = var.cluster_id != null ? 1 : 0
}


resource "random_id" "sa" {
  byte_length = 1
  prefix      = "irsa-"
}
