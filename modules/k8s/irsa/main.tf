module "oidc_role" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "4.7.0"
  create_role                   = true
  force_detach_policies         = true
  role_name                     = var.role_name
  provider_url                  = local.provider_url
  role_policy_arns              = concat([aws_iam_policy.role.arn], var.extra_policy_arn)
  oidc_fully_qualified_subjects = local.namespace == "*" ? [] : [local.role_subject]
  oidc_subjects_with_wildcards  = local.namespace != "*" ? [] : [local.role_subject]
  # oidc_fully_qualified_audiences = ["sts.amazonaws.com.cn", "sts.amazonaws.com"]
}

resource "random_id" "policy" {
  keepers = {
    policy_document = var.policy_document
  }

  byte_length = 2
}

resource "aws_iam_policy" "role" {
  name        = "${var.role_name}-${random_id.policy.id}"
  description = "policy for ${var.role_name}"
  policy      = random_id.policy.keepers.policy_document
  lifecycle {
    create_before_destroy = true
  }
}
