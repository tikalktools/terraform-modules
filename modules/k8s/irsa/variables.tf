
variable "policy_document" {
  type        = string
  description = "json formated policy for the irsa role"
}

variable "cluster_id" {
  type        = string
  default     = null
  description = "id for the cluster to get the issuer"
}

variable "provider_url" {
  type        = string
  default     = null
  description = "url for the issuer. overrides data retrived by cluster_id"
}


variable "irsa_namespace_wildcard" {
  type        = bool
  default     = false
  description = "will use '*' as namespace indicator to irsa. will allow any service account with that name in the cluster to assume the role"
}


variable "namespace" {
  type        = string
  default     = null
  description = "namespace for irsa and service account if created. must be provided if service aacount shuld be created"
}


variable "role_name" {
  type    = string
  default = null
}

variable "service_account_name" {
  type    = string
  default = null
}

variable "create_service_account" {
  type    = bool
  default = false
}


variable "extra_policy_arn" {
  type    = list(string)
  default = []
}
