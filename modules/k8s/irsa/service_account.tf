resource "kubernetes_service_account" "irsa" {
  count = local.create_sa ? 1 : 0
  metadata {
    name      = local.sa_name
    namespace = var.namespace
    annotations = {
      "${local.sa_annotation}" = module.oidc_role.iam_role_arn
    }
  }
}
