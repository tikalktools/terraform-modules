output "oidc_role" {
  value = module.oidc_role
}

output "service_account" {
  value = {
    created = local.create_sa
    name    = local.sa_name
  }
}


output "annotation" {
  value = local.sa_annotation
}

output "role_arn" {
  value = module.oidc_role.iam_role_arn
}


