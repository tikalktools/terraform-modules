resource "aws_iam_policy" "create" {
  count       = length(var.create_policies)
  name        = var.create_policies[count.index].name
  description = var.create_policies[count.index].description
  policy      = var.create_policies[count.index].policy_json
}


resource "aws_iam_role_policy_attachment" "create" {
  count      = length(var.create_policies)
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.create[count.index].arn
}
