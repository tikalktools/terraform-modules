locals {
  sa_annotation     = "eks.amazonaws.com/role-arn"
  sa_set_annotation = "serviceAccount.annotations.\\eks\\.amazonaws\\.com/role-arn"
  role_subject      = "system:serviceaccount:${local.service_account.namespace}:${local.service_account.name}"

  partition  = data.aws_partition.current.partition
  account_id = data.aws_caller_identity.current.account_id

  role_arn = "arn:${local.partition}:iam::${local.account_id}:role/${var.name}"
  # using local for future manipulation of input
  service_account = {
    create    = var.service_account.create
    name      = var.service_account.name
    namespace = var.service_account.namespace
  }
  temp_issuer = replace(var.openid_provider.issuer, "https://", "")
  openid_provider = {
    issuer = local.temp_issuer
    arn    = "arn:${data.aws_partition.current.partition}:iam::${data.aws_caller_identity.current.account_id}:oidc-provider/${local.temp_issuer}"
  }
}
