variable "create_policies" {
  type = list(object({
    name        = string
    policy_json = string
    description = string
  }))
  default     = []
  description = "list of policies to create and attach to the irsa role"
}


variable "attach_policies" {
  type        = list(string)
  default     = []
  description = "list of existing policies to attach and attach to the irsa role"
}



variable "service_account" {
  type = object({
    create    = bool
    name      = string
    namespace = string
  })
  description = "the service account details to use for the irsa role"
}


variable "openid_provider" {
  type = object({
    issuer = string
  })
  description = "[to fill]"
}

variable "name" {
  type = string
}

