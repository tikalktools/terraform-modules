resource "aws_iam_role" "this" {
  name               = var.name
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

data "aws_iam_policy_document" "assume_role" {
  version = "2012-10-17"
  statement {
    effect = "Allow"
    sid    = "base1"
    actions = [
      "sts:AssumeRoleWithWebIdentity",
      "sts:TagSession"
    ]

    principals {
      type        = "Federated"
      identifiers = [local.openid_provider.arn]
    }

    condition {
      test     = "StringLike"
      variable = "${local.openid_provider.issuer}:sub"
      values   = [local.role_subject]
    }
  }
}



