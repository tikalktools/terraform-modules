######### Default data #########
# aws; aws-cn; aws-us-gov
data "aws_partition" "current" {} # data.aws_partition.current.partition

# us-east-1; eu-west-2 etc.
data "aws_region" "current" {} # data.aws_region.current.name

# account and identifier
data "aws_caller_identity" "current" {} # data.aws_caller_identity.current.account_id
################################
