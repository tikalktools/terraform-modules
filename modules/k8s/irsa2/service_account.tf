resource "kubernetes_service_account" "irsa" {
  count = local.service_account.create ? 1 : 0
  metadata {
    name      = local.service_account.name
    namespace = local.service_account.namespace
    annotations = {
      "${local.sa_annotation}" = aws_iam_role.this.arn
    }
  }

  automount_service_account_token = true
}
