provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

provider "aws" {
  region = "us-east-1"

  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
}


data "aws_eks_cluster" "eks" {
  name = "cwp"
}

data "aws_eks_cluster_auth" "eks" {
  name = "cwp"
}

variable "tenet" {
  default = "devops"

}



module "irsa" {
  source = "../"

  name = "test1"
  create_policies = {
    "${var.tenet}-test1" = {
      policy_json = local.policy_json
      description = "test 1"
    }
  }


  attach_policies = ["arn:aws:iam::aws:policy/AmazonKinesisFirehoseFullAccess"]

  service_account = {
    create    = var.create_service_account
    name      = "satest"
    namespace = "default"
  }

  openid_provider = {
    issuer = data.aws_eks_cluster.eks.identity[0].oidc[0].issuer
  }
}



locals {
  policy_json = file("ssm_policy.json")
}



