output "role" {
  value = {
    name = aws_iam_role.this.name
    arn  = aws_iam_role.this.arn
  }
}


output "service_account" {
  value = {
    create    = local.service_account.create
    name      = local.service_account.name
    namespace = local.service_account.namespace
  }
}




